let trainer = {};

trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"]
}
trainer.talk = function(talk){
    console.log("Pikachu! I choose you!");
};
console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracet notation: ");
console.log(trainer["pokemon"]);
console.log("Result of talk method: ");
trainer.talk();